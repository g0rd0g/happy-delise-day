<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\MessagesController;
use App\Http\Controllers\ImagesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Route::prefix('messages')->middleware(['auth'])->group(function () {
    Route::get('/', [MessagesController::class, 'index'])
        ->name('messages');
    Route::get('/create', [MessagesController::class, 'create'])
        ->name('message-create');
    Route::get('/edit/{id}', [MessagesController::class, 'edit'])
        ->name('message-edit');
    Route::get('/{id}', [MessagesController::class, 'show'])
        ->name('message-show');
});

Route::prefix('archive')->middleware(['auth'])->group(function () {
    Route::get('/', [MessagesController::class, 'archive'])
        ->name('archive');
    Route::get('/{id}', [MessagesController::class, 'archiveShow'])
        ->name('archive-show');
});

Route::prefix('ajax')->middleware(['auth'])->group(function () {
    Route::post('/upload', [ImagesController::class, 'upload'])
        ->name('upload');

    Route::post('/save', [MessagesController::class, 'ajaxSave'])
        ->name('ajax-save');

    Route::post('/publish', [MessagesController::class, 'ajaxPublish'])
        ->name('ajax-publish');

    Route::post('/delete', [MessagesController::class, 'delete'])
        ->name('ajax-delete');
});


require __DIR__.'/auth.php';
