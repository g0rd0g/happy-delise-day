<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Alsofronie\Uuid\UuidModelTrait;
use App\Traits\WithUuid;

use App\Models\User;

class Message extends Model
{
    use WithUuid;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'content_json',
        'content_html',
        'shown',
        'is_next',
        'published'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
