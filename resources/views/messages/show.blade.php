<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            A message from {{ $message->user->name }}
            {{-- , {{ \Carbon\Carbon::parse($message->updated_at)->timezone('Africa/Johannesburg')->format('Y-m-d') }} --}}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200 style-container">
                    {!! $message->content_html !!}
                </div>
            </div>
        </div>
    </div>
    
</x-app-layout>
