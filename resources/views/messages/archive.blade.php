<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('All my messages') }}
        </h2>
    </x-slot>

    <div class="py-3">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200 style-container">
                @foreach ($messages as $message)
                    <div class="flex my-2">
                        <div>
                            <a href="/messages/edit/{{ $message->uuid }}" style="max-height: 100px; overflow: hidden;">
                                <div>
                                    {!! $message->content_html !!}
                                </div>
                            </a>
                        </div>
                        <div class="ml-auto pl-8 pr-5 py-5 bg-pink-200">
                            Shown: {{ $message->shown }} 
                            <a class="ml-auto" href="{{ route('archive-show', $message->uuid) }}">
                                <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded">
                                    View
                                </button>
                            </a>
                        </div>
                    </div>
                    <hr>
                @endforeach
                </div>
            </div>
        </div>
    </div>
    
</x-app-layout>
