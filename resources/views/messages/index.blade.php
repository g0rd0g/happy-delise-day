<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('All my messages') }}
        </h2>
        {{-- <h2 class="ml-auto font-semibold text-xl text-gray-800 leading-tight">
            A total of <span>{{ $total_messages }}</span> messages have been created so far !
        </h2> --}}
    </x-slot>

    <div class="pt-12 pb-6">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="flex align-items-center p-6 bg-white border-b border-gray-200">
                    <h3>
                        Hi {{ Auth::user()->name }}!💫
                    </h3>
                    <a class="ml-auto" href="/messages/create">
                        <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded">
                            Create a new message
                        </button>
                    </a>
                </div>
                <div class="flex align-items-center p-6 bg-white border-b border-gray-200">
                    How it works:
                    <br>
                    Delise saw an instagram post a few month back that she LOVED 🌈. Basically in this insta post, a birthday gal's group of friends created 365 messages 
                    between them as a present so that she would have one to read everyday until her next birthday. So I've invited her 
                    favourite people here to help make this happen ! (or atleast try to get close to 365 :P) 🎉🎉✍️ The messages don't have to be long, they can be any length and you can make as many as you like.
                    Maybe a memory the 2 of you shared, a little message with a link to a song, a photo of you 2 together with a caption. Or even a whole essay if you're feeling up to it. It's up to you 🌈.
                    <br>
                    So everyday, from the 1st of April, a random message that she hasn't seen yet will become available for her to see 🌞 (When she logs in she will see a different screen that shows her the message of the day).
                    <br>
                    <br>
                    You can create messages at anytime - even after her birthday has passed so please don't stop after the 31st ✍️✍️✍️🌸❗❗
                    <br>
                    <br>
                    P.S only you and Delise can see your messages but she will only get to see them from the day they are shown. Also, only published messages will get shown so don't forget to click publish when you're done!
                    <br>
                    Also, if you have any questions just message me @whakeenfenix on insta if you don't have my number :P
                </div>
            </div>
        </div>
    </div>

    <div class="py-3">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200 style-container">
                    @if ($messages->count() == 0)
                        No messages yet! Create one by clicking the button on the right. @endif
                        @foreach ($messages as $message)
                        <div class="flex my-2">
                            <div>
                                <a href="/messages/edit/{{ $message->uuid }}" style="max-height: 100px; overflow: hidden;">
                                    <div>
                                        {!! $message->content_html !!}
                                    </div>
                                </a>
                            </div>
                            <div class="ml-auto pl-8 pr-5 py-5 bg-pink-200">
                                @if ($message->published)
                                    @if ($message->shown)
                                        @if ($message->shown == $todays_date)
                                            Showing today!!
                                        @else
                                            Shown: {{ $message->shown }} 
                                        @endif
                                    @else
                                        Not shown yet
                                    @endif
                                @else
                                    <a class="ml-auto" href="/messages/edit/{{ $message->uuid }}">
                                        <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded">
                                            Edit
                                        </button>
                                    </a>
                                @endif
                            </div>
                        </div>
                        <hr>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
    
</x-app-layout>
