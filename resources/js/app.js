require('./bootstrap');

import { createApp } from 'vue';

import TipTap from './TipTap.vue';

import Alpine from 'alpinejs';

window.Alpine = Alpine;

Alpine.start();

const app = createApp({});

app.component('tip-tap', TipTap);

if (document.getElementById('app')) {
    app.mount('#app')
}

