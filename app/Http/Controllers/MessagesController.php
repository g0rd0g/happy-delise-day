<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;

class MessagesController extends Controller
{
    public function index()
    {
        if (Auth::user()->role == 'birthday gal') {
            $time_until_next_message = Carbon::now()->timezone('Africa/Johannesburg')->endOfDay()->diffInSeconds(Carbon::now()->timezone('Africa/Johannesburg'));
            $todays_date = Carbon::now()->timezone('Africa/Johannesburg')->format('Y-m-d');
            $message = Message::where('shown', $todays_date)->where('published', true)->first();
            if (!$message) {
                $message = Message::whereNull('shown')->where('published', true)->inRandomOrder()->first();

                if ($message) {
                    $message->shown = $todays_date;
                    $message->first_shown = $todays_date;
                    $message->save();
                }

                if (!$message) {
                    $message = Message::where('published', true)->inRandomOrder()->first();
                    $message->shown = $todays_date;
                    $message->save();
                }
            }

            return view('messages.show', compact('message', 'todays_date', 'time_until_next_message'));
        } else {
            $total_messages = Message::where('published', true)->count();
            $time_until_next_message = Carbon::now()->timezone('Africa/Johannesburg')->endOfDay()->diffInSeconds(Carbon::now()->timezone('Africa/Johannesburg'));
            $todays_date = Carbon::now()->timezone('Africa/Johannesburg')->format('Y-m-d');
            $messages = Message::where('user_id', Auth::id())->get();

            return view('messages.index', compact('messages', 'todays_date', 'total_messages'));
        }
    }

    public function create()
    {
        $message = Message::create([
            'user_id' => Auth::id(),
            'content_json' => '{}',
            'content_html' => ''
        ]);

        return redirect('/messages/edit/' . $message->uuid);
    }

    public function show($message_id)
    {
        if (Auth::user()->role == 'birthday gal') {
            $message = Message::where('uuid', $message_id)->where('user_id', Auth::id())->first();
            return view('messages.show', compact('message'));
        }
        return redirect()->back();
    }

    public function edit($message_id)
    {
        $message = Message::where('uuid', $message_id)->where('user_id', Auth::id())->first();

        return view('messages.edit', compact('message'));
    }

    public function preview($message_id)
    {
        $message = Message::where('uuid', $message_id)->where('user_id', Auth::id())->first();
    }

    public function ajaxSave(Request $request)
    {
        $content_json = $request->input('content_json');
        $content_html = $request->input('content_html');
        $message_id = $request->input('message_id');

        $message = Message::where('uuid', $message_id)->where('user_id', Auth::id())->update([
            'content_json' => $content_json,
            'content_html' => $content_html
        ]);

        return ['success' => true];
    }

    public function ajaxPublish(Request $request)
    {
        $content_json = $request->input('content_json');
        $content_html = $request->input('content_html');
        $message_id = $request->input('message_id');

        $message = Message::where('uuid', $message_id)->where('user_id', Auth::id())->update([
            'content_json' => $content_json,
            'content_html' => $content_html,
            'published' => true 
        ]);

        return ['success' => true];
    }

    public function delete(Request $request)
    {
        $message_id = $request->input('message_id');
        $message = Message::where('published', false)->where('uuid', $message_id)->where('user_id', Auth::id())->delete();

        return ['success' => true];
    }

    public function archive()
    {
        if (Auth::user()->role == 'birthday gal') {
            $messages = Message::whereNotNull('shown')->orderBy('shown')->get();
            return view('messages.archive', compact('messages'));
        }

        return redirect()->back();
    }

    public function archiveShow($message_id)
    {
        if (Auth::user()->role == 'birthday gal') {
            $message = Message::where('uuid', $message_id)->first();
            return view('messages.archive-show', compact('message'));
        }

        return redirect()->back();
    }
}
