const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    content: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: ['latoregular', ...defaultTheme.fontFamily.sans],
            },
            colors: {
                'delise-pink': '#ff7c98',
            },
            fontSize: {
                '15xl': '20rem'
            }
        },
    },

    plugins: [require('@tailwindcss/forms')],
};
