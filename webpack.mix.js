const mix = require('laravel-mix');

// mix.webpackConfig({
//     stats: {
//         children: true
//     }
// });

mix.js('resources/js/app.js', 'public/js')
    .vue()
    .sass('resources/css/styles.scss', '/public/css')
    .postCss('resources/css/app.css', 'public/css/concat.css', [
        require('postcss-import'),
        require('tailwindcss'),
        require('autoprefixer'),
    ]
)
.copyDirectory('resources/fonts', 'public/fonts');

mix.combine([
    'public/css/*.css'
],'public/css/app.css');
