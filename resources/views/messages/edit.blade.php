<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Message') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    Hi {{ Auth::user()->name }}! Edit your message below and don't forget to <strong>save</strong> so that you can come back to it, or <strong>publish</strong> once you're done. To go back to the home screen click the heart or 'my messages' in the top left corner. 
                </div>
            </div>
        </div>
    </div>

    <div class="py-3">
        <div class="max-w-1lg mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div id="app">
                        <div class="ProseMirror">
                            <tip-tap content="{{ $message->content_json }}"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</x-app-layout>
